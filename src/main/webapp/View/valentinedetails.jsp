<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Valentine Days</title>
</head>
<body>
	<h1 style="text-align: center;">Valentine Details</h1><br/><br/>
	<b>Date:</b> ${valentine.date}<br><Br/>
	<b>Gifts: </b> <Br/>
	  <c:forEach var = "i" items="${valentine.gifts}">
         <c:out value = "${i}"/><Br/>
      </c:forEach>
      <br/>
	<b>Location:</b> ${valentine.location}
	
</body>
</html>