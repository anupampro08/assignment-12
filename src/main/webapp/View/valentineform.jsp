<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>  
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Valentine Form</title>
</head>
<body>
	<h1 style="text-align: center;">Happy Valentine's Day</h1>
	<form:form action="valentinedetails" modelAttribute="valentine">
	<br/>
		Date:<form:input path="date"/><br><br/>
		Gifts:<form:checkboxes items="${gifts}" path="gifts"/><br><br/>
		Location<form:input path="location"/><br><br>
		<input type="submit" value="submit">
	</form:form>
</body>
</html>